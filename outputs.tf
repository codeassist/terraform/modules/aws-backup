output "backup_vault_id" {
  description = "Backup Vault ID."
  value       = join("", aws_backup_vault.this.*.id)
}

output "backup_vault_arn" {
  description = "Backup Vault ARN."
  value       = join("", aws_backup_vault.this.*.arn)
}

output "backup_vault_recovery_points" {
  description = "Backup Vault recovery points."
  value       = join("", aws_backup_vault.this.*.recovery_points)
}

output "backup_plan_arn" {
  description = "Backup Plan ARN."
  value       = join("", aws_backup_plan.this.*.arn)
}

output "backup_plan_version" {
  description = "Unique, randomly generated, Unicode, UTF-8 encoded string that serves as the version ID of the backup plan."
  value       = join("", aws_backup_plan.this.*.version)
}

output "backup_selection_resources_by_id" {
  description = "Backup Selection ID."
  value       = join("", aws_backup_selection.resources_by_id.*.id)
}
output "backup_selection_resources_by_tags" {
  description = "Backup Selection ID."
  value       = join("", aws_backup_selection.resources_by_tags.*.id)
}
