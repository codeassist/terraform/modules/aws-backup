locals {
  enabled = var.backup_module_enabled ? true : false
  tags = merge(
    var.backup_tags,
    {
      terraform = true
    }
  )
}

data "aws_iam_policy_document" "assume_role" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  statement {
    sid = "AllowBackupServiceAssumeThisRole"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
    effect = "Allow"
  }
}
resource "aws_iam_role" "backup" {
  # Provides an IAM role.

  # NOTE: If policies are attached to the role via the `aws_iam_policy_attachment` resource and you are modifying the
  # role `name` or `path`, the `force_detach_policies` argument must be set to `true` and applied before attempting the
  # operation otherwise you will encounter a `DeleteConflict` error.
  # The `aws_iam_role_policy_attachment` resource (recommended) does not have this requirement.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-backup-role-", var.backup_common_name)
  # (Optional) The description of the role.
  description = "The IAM role for AWS Backup [${var.backup_common_name}] service."
  # Key-value mapping of tags for the IAM role.
  tags = merge(
    local.tags,
    {
      Name = format("%s-backup-role", var.backup_common_name)
    },
  )
  # (Optional) The path to the role. See IAM Identifiers for more information:
  #   * https://docs.aws.amazon.com/IAM/latest/UserGuide/Using_Identifiers.html
  path = "/"
  # (Required) The policy that grants an entity permission to assume the role.
  assume_role_policy = join("", data.aws_iam_policy_document.assume_role.*.json)
}
resource "aws_iam_role_policy_attachment" "backup_service" {
  # Attaches a Managed IAM Policy to an IAM role.
  # NOTE(!): the usage of this resource conflicts with the aws_iam_policy_attachment resource and will permanently
  # show a difference if both are defined.
  count = local.enabled ? 1 : 0
  # (Required) The role the policy should be applied to.
  role = join("", aws_iam_role.backup.*.name)
  # (Required) The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
}

# Workaround to be able to remove vault since AWS API doesn't allow to remove a vault if there Recovery points are
# present.
resource "null_resource" "vault_cleanup" {
  # The null_resource resource implements the standard resource lifecycle but takes no further action.
  count = local.enabled ? 1 : 0

  depends_on = [
    aws_backup_vault.this
  ]

  # The triggers argument allows specifying an arbitrary set of values that, when changed, will cause the resource
  # to be replaced.
  triggers = {
    aws_region = var.backup_current_region
    backup_vault_name = join("", aws_backup_vault.this.*.name)
  }

  provisioner "local-exec" {
    # If when = "destroy" is specified, the provisioner will run when the resource it is defined within is destroyed.
    when = destroy

    # (Required) This is the command to execute. It can be provided as a relative path to the current working
    # directory or as an absolute path. It is evaluated in a shell, and can use environment variables or
    # Terraform variables.
    command = <<-COMMAND
        aws backup list-recovery-points-by-backup-vault \
            --backup-vault-name ${self.triggers.backup_vault_name} \
            --query 'RecoveryPoints[*].RecoveryPointArn | join(`"\n"`, @)' --output text \
          | xargs -t -I {RECOVERY_POINT_ARN} aws backup delete-recovery-point \
                                              --backup-vault-name ${self.triggers.backup_vault_name} \
                                              --recovery-point-arn {RECOVERY_POINT_ARN} \
        && sleep 30;
    COMMAND
    # (Optional) block of key value pairs representing the environment of the executed command.
    # Inherits the current process environment.
    environment = {
      AWS_DEFAULT_REGION = self.triggers.aws_region
      AWS_REGION         = self.triggers.aws_region
    }

    # By default, provisioners that fail will also cause the Terraform apply itself to fail. The `on_failure` setting
    # can be used to change this.
    on_failure = continue
  }
}

resource "aws_backup_vault" "this" {
  # Provides an AWS Backup vault resource.
  count = local.enabled ? 1 : 0

  # (Required) Name of the backup vault to create.
  name = format("%s-backup-vault", var.backup_common_name)
  # (Optional) Metadata that you can assign to help organize the resources that you create.
  tags = merge(
    local.tags,
    {
      Name = format("%s-backup-vault", var.backup_common_name)
    }
  )
  # (Optional) The server-side encryption key that is used to protect your backups.
  kms_key_arn = var.backup_kms_key_arn
}

resource "aws_backup_plan" "this" {
  # Provides an AWS Backup plan resource.
  count = local.enabled ? 1 : 0

  # (Required) The display name of a backup plan.
  name  = format("%s-backup-plan", var.backup_common_name)

  # (Required) A rule object that specifies a scheduled task that is used to back up a selection of resources.
  rule {
    # (Required) An display name for a backup rule.
    rule_name           = format("%s-rule", join("", aws_backup_vault.this.*.name))
    # (Required) - The name of a logical container where backups are stored.
    target_vault_name   = join("", aws_backup_vault.this.*.name)
    # A CRON expression specifying when AWS Backup initiates a backup job.
    schedule            = var.backup_schedule
    # The amount of time in minutes before beginning a backup.
    start_window        = var.backup_start_window
    # The amount of time AWS Backup attempts a backup before canceling the job and returning an error.
    completion_window   = var.backup_completion_window

    # The lifecycle defines when a protected resource is transitioned to cold storage and when it expires.
    dynamic "lifecycle" {
      for_each = (var.backup_lifecycle_cold_storage_after != 0 && var.backup_lifecycle_delete_after != 0) ? ["true"] : []
      content {
        # Specifies the number of days after creation that a recovery point is moved to cold storage.
        cold_storage_after = var.backup_lifecycle_cold_storage_after
        # Specifies the number of days after creation that a recovery point is deleted. (Must be 90 days greater than
        # `cold_storage_after`).
        delete_after       = var.backup_lifecycle_delete_after
      }
    }

    dynamic "lifecycle" {
      for_each = (var.backup_lifecycle_cold_storage_after != 0 && var.backup_lifecycle_delete_after == 0) ? ["true"] : []
      content {
        cold_storage_after = var.backup_lifecycle_cold_storage_after
      }
    }

    dynamic "lifecycle" {
      for_each = (var.backup_lifecycle_cold_storage_after == 0 && var.backup_lifecycle_delete_after != 0) ? ["true"] : []
      content {
        delete_after       = var.backup_lifecycle_delete_after
      }
    }

    # Metadata that you can assign to help organize the resources that you create.
    recovery_point_tags = merge(
      local.tags,
      var.backup_recovery_point_tags
    )

    # Configuration block(s) with copy operation settings.
    dynamic "copy_action" {
      for_each = var.backup_copy_destination_vault_arn != "" ? ["true"] : []
      content {
        # on June, 2020 a `lifecycle` block is not optional, but mandatory, see:
        #   * https://github.com/terraform-providers/terraform-provider-aws/issues/13165
        # check if user has not changed the lifecycle variables then set meaningless values (~ 100 years) to both of
        # them
        lifecycle {
          # Specifies the number of days after creation that a recovery point is moved to cold storage.
          cold_storage_after = var.backup_lifecycle_cold_storage_after != 0 ? var.backup_lifecycle_cold_storage_after : (var.backup_lifecycle_delete_after != 0 ? (var.backup_lifecycle_delete_after - 90) : 36400)
          # Specifies the number of days after creation that a recovery point is deleted. (Must be 90 days greater than
          # `cold_storage_after`).
          delete_after       = var.backup_lifecycle_delete_after != 0 ? var.backup_lifecycle_delete_after : 36500
        }

        # (Required) An Amazon Resource Name (ARN) that uniquely identifies the destination backup vault for the copied
        # backup.
        destination_vault_arn = var.backup_copy_destination_vault_arn
      }
    }
  }

  # (Optional) Metadata that you can assign to help organize the plans you create.
  tags = merge(
    local.tags,
    {
      Name = format("%s-backup-plan", var.backup_common_name)
    }
  )
}

resource "aws_backup_selection" "resources_by_id" {
  # Manages selection conditions for AWS Backup plan resources.
  count        = (local.enabled && length(var.backup_resources) > 0) ? 1 : 0

  # (Required) The display name of a resource selection document.
  name         = format("%s-selection-by-id", var.backup_common_name)
  # (Required) The backup plan ID to be associated with the selection of resources.
  plan_id      = join("", aws_backup_plan.this.*.id)
  # (Required) The ARN of the IAM role that AWS Backup uses to authenticate when restoring and backing up the target
  # resource.
  iam_role_arn = join("", aws_iam_role.backup.*.arn)
  # An array of strings that either contain Amazon Resource Names (ARNs) or match patterns of resources to assign
  # to a backup plan.
  resources    = var.backup_resources

  # make a small dealy to avoid race condition failure
  provisioner "local-exec" {
    when    = destroy
    command = "sleep 10;"
  }
}
resource "aws_backup_selection" "resources_by_tags" {
  # Manages selection conditions for AWS Backup plan resources.
  count        = (local.enabled && var.backup_selection_tags != null) ? 1 : 0

  # (Required) The display name of a resource selection document.
  name         = format("%s-selection-by-tags", var.backup_common_name)
  # (Required) The backup plan ID to be associated with the selection of resources.
  plan_id      = join("", aws_backup_plan.this.*.id)
  # (Required) The ARN of the IAM role that AWS Backup uses to authenticate when restoring and backing up the target
  # resource.
  iam_role_arn = join("", aws_iam_role.backup.*.arn)
  # (Optional) Tag-based conditions used to specify a set of resources to assign to a backup plan.
  dynamic "selection_tag" {
    for_each = var.backup_selection_tags != null ? var.backup_selection_tags : []
    content {
      type = lookup(selection_tag.value, "type", "STRINGEQUALS")
      key = lookup(selection_tag.value, "key", "")
      value = lookup(selection_tag.value, "value", "")
    }
  }

  # make a small dealy to avoid race condition failure
  provisioner "local-exec" {
    when    = destroy
    command = "sleep 10;"
  }
}
