# Whether to create the resources ("false" prevents the module from creating any resources).
backup_module_enabled = true
# (Required) Current region where all the resources are being managed.
backup_current_region = "us-east-1"
# A mapping of common tags to assign to the resources.
backup_tags = {}
# (Required) A name associated with the Backup resources to distinct from others.
backup_common_name = "test4"
# The server-side encryption key that is used to protect your backups.
backup_kms_key_arn = null
# A CRON expression specifying when AWS Backup initiates a backup job.
backup_schedule = null
# The amount of time in minutes before beginning a backup (minimum value is 60 minutes).
backup_start_window = null
# The amount of time AWS Backup attempts a backup before canceling the job and returning an error. Must be
# at least 60 minutes greater than `start_window`!
backup_completion_window = null
# Specifies the number of days after creation that a recovery point is moved to cold storage.
backup_lifecycle_cold_storage_after = 0
# Specifies the number of days after creation that a recovery point is deleted. Must be 90 days greater
# than `cold_storage_after`.
backup_lifecycle_delete_after = 0
# ARN that uniquely identifies the destination backup vault for the copied backup.
backup_copy_destination_vault_arn = ""
# Metadata that you can assign to help organize the resources that you create.
backup_recovery_point_tags = {
  backup = true
}
# (Required) An array of strings that either contain Amazon Resource Names (ARNs) or match patterns of resources
# to assign to a backup plan.
backup_resources = [
  "arn:test1"
]
# Tag-based conditions used to specify a set of resources to assign to a backup plan.
backup_selection_tags = [
  {
    type = "STRINGEQUALS"
    key = "TestTagKey"
    value = "TestTagValue"
  }
]
