variable "backup_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # "false" prevents the module from creating any resources
  default = false
}

variable "backup_current_region" {
  description = "(Required) Current region where all the resources are being managed."
  type        = string
}

variable "backup_tags" {
  description = "A mapping of common tags to assign to the resources."
  type        = map(string)
  default     = {}
}

variable "backup_common_name" {
  description = "(Required) A name associated with the Backup resources to distinct from others."
  type        = string
}

variable "backup_kms_key_arn" {
  description = "The server-side encryption key that is used to protect your backups."
  type        = string
  default     = null
}

variable "backup_schedule" {
  description = "A CRON expression specifying when AWS Backup initiates a backup job."
  type        = string
  default     = null
}

variable "backup_start_window" {
  description = "The amount of time in minutes before beginning a backup (minimum value is 60 minutes)."
  type        = number
  default     = null
}
variable "backup_completion_window" {
  description = "The amount of time AWS Backup attempts a backup before canceling the job and returning an error. Must be at least 60 minutes greater than `start_window`!"
  type        = number
  default     = null
}

variable "backup_lifecycle_cold_storage_after" {
  description = "Specifies the number of days after creation that a recovery point is moved to cold storage."
  type        = number
  default     = 0
}
variable "backup_lifecycle_delete_after" {
  description = "Specifies the number of days after creation that a recovery point is deleted. Must be 90 days greater than `cold_storage_after`."
  type        = number
  default     = 0
}

variable "backup_recovery_point_tags" {
  description = "Metadata that you can assign to help organize the resources that you create."
  type = map(string)
  default = {}
}

variable "backup_copy_destination_vault_arn" {
  description = "ARN that uniquely identifies the destination backup vault for the copied backup."
  type        = string
  default     = ""
}

# One of the next variables must be non-empty

variable "backup_resources" {
  description = "An array of strings that either contain Amazon Resource Names (ARNs) or match patterns of resources to assign to a backup plan."
  type        = list(string)
  default     = []
}
variable "backup_selection_tags" {
  description = "Tag-based conditions used to specify a set of resources to assign to a backup plan."
  type        = list(object({
    # (Required) An operation, such as StringEquals, that is applied to a key-value pair used to filter resources
    # in a selection.
    type = string
    # (Required) The key in a key-value pair.
    key = string
    # (Required) The value in a key-value pair.
    value = string
  }))
  default = null
}
